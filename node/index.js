const express = require('express')
const app = express()
const PORT = 8080

app.listen(PORT, () => {
  console.log('Server listening on port: ' + PORT)
})

app.get('/', function (req, res, next) {
  res.sendFile(__dirname + '/public/index.html')
  console.log('test')
})

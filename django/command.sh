#!/usr/bin/env bash
set -e

cd /code

python3 manage.py migrate --noinput
